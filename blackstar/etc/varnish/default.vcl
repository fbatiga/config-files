vcl 4.0;

import std;
import directors;

backend default {
    .host = "tatooine";
    .port = "80";
#    .host = "40.114.242.27";
#    .port = "8080";

}

backend api_host {
    .host = "darthvader";
    .port = "1975";
.connect_timeout = 600s;
.first_byte_timeout = 600s;
.between_bytes_timeout = 600s;
.probe = {
                .url = "/REST/status";
                .interval = 5s;
                .timeout = 1s;
                .window = 5;
                .threshold = 3;
  }
}


backend api_host2 {
    .host = "darthvader";
    .port = "1976";
.connect_timeout = 600s;
.first_byte_timeout = 600s;
.between_bytes_timeout = 600s;
.probe = {
                .url = "/REST/status";
                .interval = 3s;
                .timeout = 1s;
                .window = 5;
                .threshold = 3;
  }
}

backend api_host3 {
    .host = "darthvader";
    .port = "1977";
.connect_timeout = 600s;
.first_byte_timeout = 600s;
.between_bytes_timeout = 600s;
.probe = {
                .url = "/REST/status";
                .interval = 5s;
                .timeout = 1s;
                .window = 5;
                .threshold = 3;
  }
}


backend api_host_vyvop {
    .host = "darthvader";
    .port = "1978";
.connect_timeout = 600s;
.first_byte_timeout = 600s;
.between_bytes_timeout = 600s;
.probe = {
                .url = "/REST/status";
                .interval = 5s;
                .timeout = 1s;
                .window = 5;
                .threshold = 3;
  }
}





backend api_blog_host {
    .host = "darthvader";
    .port = "2368";
}

backend api_yellow_host {
    .host = "darthvader";
    .port = "2016";
}

backend api_pm2_host {
    .host = "darthvader";
    .port = "8088";
}


#backend feedback_host {
#    .host = "10.0.0.80";
#    .port = "80";
#}


backend hcombined_host {
    .host = "brands.datahc.com";
    .port = "80";
}


#backend ovh_host {
#    .host = "87.98.163.60";
#    .port = "80";
#}


# 
# Below is a commented-out copy of the default VCL logic.  If you
# redefine any of these subroutines, the built-in logic will be
# appended to your code.
# sub vcl_recv {
#     if (req.restarts == 0) {
# 	if (req.http.x-forwarded-for) {
# 	    set req.http.X-Forwarded-For =
# 		req.http.X-Forwarded-For + ", " + client.ip;
# 	} else {
# 	    set req.http.X-Forwarded-For = client.ip;
# 	}
#     }
#     if (req.method != "GET" &&
#       req.method != "HEAD" &&
#       req.method != "PUT" &&
#       req.method != "POST" &&
#       req.method != "TRACE" &&
#       req.method != "OPTIONS" &&
#       req.method != "DELETE") {
#         /* Non-RFC2616 or CONNECT which is weird. */
#         return (pipe);
#     }
#     if (req.method != "GET" && req.method != "HEAD") {
#         /* We only deal with GET and HEAD by default */
#         return (pass);
#     }
#     if (req.http.Authorization || req.http.Cookie) {
#         /* Not cacheable by default */
#         return (pass);
#     }
#     return (lookup);
#}


sub vcl_init {
    new kwgBalancer = directors.round_robin();

    kwgBalancer.add_backend(api_host);
    kwgBalancer.add_backend(api_host2);
}




sub vcl_recv {


if (req.http.Accept-Encoding) {
        if (req.http.Accept-Encoding ~ "gzip") {
            # If the browser supports it, we'll use gzip.
            set req.http.Accept-Encoding = "gzip";
        }
        else if (req.http.Accept-Encoding ~ "deflate") {
            # Next, try deflate if it is supported.
            set req.http.Accept-Encoding = "deflate";
        }
        else {
            # Unknown algorithm. Remove it and send unencoded.
            unset req.http.Accept-Encoding;
        }
    }

if (req.restarts == 0) {
        if (req.http.x-forwarded-for) {
            set req.http.X-Forwarded-For =
                req.http.X-Forwarded-For + ", " + client.ip;
        } else {
            set req.http.X-Forwarded-For = client.ip;
        }
 }


if(
      req.http.user-agent ~ "^$"
      || req.http.user-agent ~ "^Java"
      || req.http.user-agent ~ "^Jakarta"
      || req.http.user-agent ~ "IDBot"
      || req.http.user-agent ~ "id-search"
      || req.http.user-agent ~ "User-Agent"
      || req.http.user-agent ~ "compatible ;"
      || req.http.user-agent ~ "ConveraCrawler"
      || req.http.user-agent ~ "^Mozilla$"
      || req.http.user-agent ~ "libwww"
      || req.http.user-agent ~ "lwp-trivial"
      || req.http.user-agent ~ "curl"
      || req.http.user-agent ~ "PHP/"
      || req.http.user-agent ~ "urllib"
      || req.http.user-agent ~ "GT:WWW"
      || req.http.user-agent ~ "Snoopy"
      || req.http.user-agent ~ "MFC_Tear_Sample"
      || req.http.user-agent ~ "HTTP::Lite"
      || req.http.user-agent ~ "PHPCrawl"
      || req.http.user-agent ~ "URI::Fetch"
      || req.http.user-agent ~ "Zend_Http_Client"
      || req.http.user-agent ~ "http client"
      || req.http.user-agent ~ "PECL::HTTP"
      || req.http.user-agent ~ "panscient.com"
      || req.http.user-agent ~ "IBM EVV"
      || req.http.user-agent ~ "Bork-edition"
      || req.http.user-agent ~ "Fetch API Request"
      || req.http.user-agent ~ "PleaseCrawl"
      || req.http.user-agent ~ "[A-Z][a-z]{3,} [a-z]{4,} [a-z]{4,}"
      || req.http.user-agent ~ "layeredtech.com"
      || req.http.user-agent ~ "WEP Search"
      || req.http.user-agent ~ "Wells Search II"
      || req.http.user-agent ~ "Missigua Locator"
      || req.http.user-agent ~ "ISC Systems iRc Search 2.1"
      || req.http.user-agent ~ "Microsoft URL Control"
      || req.http.user-agent ~ "Indy Library"
      || req.http.user-agent == "8484 Boston Project v 1.0"
      || req.http.user-agent == "Atomic_Email_Hunter/4.0"
      || req.http.user-agent == "atSpider/1.0"
      || req.http.user-agent == "autoemailspider"
      || req.http.user-agent == "China Local Browse 2.6"
      || req.http.user-agent == "ContactBot/0.2"
      || req.http.user-agent == "ContentSmartz"
      || req.http.user-agent == "DataCha0s/2.0"
      || req.http.user-agent == "DataCha0s/2.0"
      || req.http.user-agent == "DBrowse 1.4b"
      || req.http.user-agent == "DBrowse 1.4d"
      || req.http.user-agent == "Demo Bot DOT 16b"
      || req.http.user-agent == "Demo Bot Z 16b"
      || req.http.user-agent == "DSurf15a 01"
      || req.http.user-agent == "DSurf15a 71"
      || req.http.user-agent == "DSurf15a 81"
      || req.http.user-agent == "DSurf15a VA"
      || req.http.user-agent == "EBrowse 1.4b"
      || req.http.user-agent == "Educate Search VxB"
      || req.http.user-agent == "EmailSiphon"
      || req.http.user-agent == "EmailWolf 1.00"
      || req.http.user-agent == "ESurf15a 15"
      || req.http.user-agent == "ExtractorPro"
      || req.http.user-agent == "Franklin Locator 1.8"
      || req.http.user-agent == "FSurf15a 01"
      || req.http.user-agent == "Full Web Bot 0416B"
      || req.http.user-agent == "Full Web Bot 0516B"
      || req.http.user-agent == "Full Web Bot 2816B"
      || req.http.user-agent == "Guestbook Auto Submitter"
      || req.http.user-agent == "Industry Program 1.0.x"
      || req.http.user-agent == "ISC Systems iRc Search 2.1"
      || req.http.user-agent == "IUPUI Research Bot v 1.9a"
      || req.http.user-agent == "LARBIN-EXPERIMENTAL (efp@gmx.net)"
      || req.http.user-agent == "LetsCrawl.com/1.0 +http://letscrawl.com/"
      || req.http.user-agent == "Lincoln State Web Browser"
      || req.http.user-agent == "LMQueueBot/0.2"
      || req.http.user-agent == "LWP::Simple/5.803"
      || req.http.user-agent == "Mac Finder 1.0.xx"
      || req.http.user-agent == "MFC Foundation Class Library 4.0"
      || req.http.user-agent == "Microsoft URL Control - 6.00.8xxx"
      || req.http.user-agent == "Missauga Locate 1.0.0"
      || req.http.user-agent == "Missigua Locator 1.9"
      || req.http.user-agent == "Missouri College Browse"
      || req.http.user-agent == "Mizzu Labs 2.2"
      || req.http.user-agent == "Mo College 1.9"
      || req.http.user-agent == "Mozilla/2.0 (compatible; NEWT ActiveX; Win32)"
      || req.http.user-agent == "Mozilla/3.0 (compatible; Indy Library)"
      || req.http.user-agent == "Mozilla/4.0 (compatible; Advanced Email Extractor v2.xx)"
      || req.http.user-agent == "Mozilla/4.0 (compatible; Iplexx Spider/1.0 http://www.iplexx.at)"
      || req.http.user-agent == "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt; DTS Agent"
      || req.http.user-agent == "Mozilla/4.0 efp@gmx.net"
      || req.http.user-agent == "Mozilla/5.0 (Version: xxxx Type:xx)"
      || req.http.user-agent == "MVAClient"
      || req.http.user-agent == "NameOfAgent (CMS Spider)"
      || req.http.user-agent == "NASA Search 1.0"
      || req.http.user-agent == "Nsauditor/1.x"
      || req.http.user-agent == "PBrowse 1.4b"
      || req.http.user-agent == "PEval 1.4b"
      || req.http.user-agent == "Poirot"
      || req.http.user-agent == "Port Huron Labs"
      || req.http.user-agent == "Production Bot 0116B"
      || req.http.user-agent == "Production Bot 2016B"
      || req.http.user-agent == "Production Bot DOT 3016B"
      || req.http.user-agent == "Program Shareware 1.0.2"
      || req.http.user-agent == "PSurf15a 11"
      || req.http.user-agent == "PSurf15a 51"
      || req.http.user-agent == "PSurf15a VA"
      || req.http.user-agent == "psycheclone"
      || req.http.user-agent == "RSurf15a 41"
      || req.http.user-agent == "RSurf15a 51"
      || req.http.user-agent == "RSurf15a 81"
      || req.http.user-agent == "searchbot admin@google.com"
      || req.http.user-agent == "ShablastBot 1.0"
      || req.http.user-agent == "snap.com beta crawler v0"
      || req.http.user-agent == "Snapbot/1.0"
      || req.http.user-agent == "sogou develop spider"
      || req.http.user-agent == "Sogou Orion spider/3.0(+http://www.sogou.com/docs/help/webmasters.htm#07)"
      || req.http.user-agent == "sogou spider"
      || req.http.user-agent == "Sogou web spider/3.0(+http://www.sogou.com/docs/help/webmasters.htm#07)"
      || req.http.user-agent == "sohu agent"
      || req.http.user-agent == "SSurf15a 11"
      || req.http.user-agent == "TSurf15a 11"
      || req.http.user-agent == "Under the Rainbow 2.2"
      || req.http.user-agent == "User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)"
      || req.http.user-agent == "VadixBot"
      || req.http.user-agent == "WebVulnCrawl.blogspot.com/1.0 libwww-perl/5.803"
      || req.http.user-agent == "Wells Search II"
      || req.http.user-agent == "WEP Search 00"
      || req.http.user-agent == "AhrefsBot/5.0"
      || req.http.user-agent ~ "Mechanize"
    ) {
return       (synth(403, "You are banned from this site.  Please contact via a different client configuration if you believe that this is a mistake."));
    }

  if(req.http.host == "" || !(req.http.host  ~ "(kanweego|kenweego|kenwego|kenwigo|canweego|keenwego|vyvop|enyosolutions)")){
	return (synth( 700, "Error") );
   }
 else if (req.url ~ "startup-life(.*)" ) {
        set req.backend_hint =   api_blog_host;
        return (pass);
  }
 else if (req.http.host ==  "yellowstart.enyosolutions.com" ) {
        set req.backend_hint =   api_yellow_host;
        return (pass);
  }
 else if (req.http.host ==  "pm2.enyosolutions.com" ) {
        set req.backend_hint =   api_pm2_host;
        return (pass);
  }
 else if (req.http.host ==  "admmm.vyvop.com" ) {
        set req.backend_hint =   api_host_vyvop;
        return (pass);
  }

 
 else if (req.url ~ "(?i)^/REST") {
    set req.backend_hint = kwgBalancer.backend();
    set req.url = regsub(req.url, "(?i)^/REST", "/REST");
	return (pass);
  }

  else if (req.url ~ "(?i)^/v2/REST") {
    set req.backend_hint = api_host3;
    set req.url = regsub(req.url, "(?i)^/v2/REST", "/REST");
        return (pass);
  }


  else if (req.http.host == "blog.kenweego.com") {
	set req.http.host = "kenweego.com";
	set req.url = "/news" + req.url; 
	return (synth(750, "http://" + req.http.host + req.url));
  }
  else if (req.url ~ "hotels.kenweego.com") {
        set req.backend_hint =   hcombined_host;
	return (pass);
  }
# else if (req.http.host ~ "blog.kenweego.com") {
#	set req.http.host = "kenweego.com";
#	set req.url =  "/news" + req.url;
#        return (pass);
#  }

  if (req.http.cookie ~ "(wordpress_logged_in|wp-settings)") {
    return(pass);
  }

    # Remove the wp-settings-1 cookie
	set req.http.Cookie = regsuball(req.http.Cookie, "wp-settings-1=[^;]+(; )?", "");
	# Remove the wp-settings-time-1 cookie
	set req.http.Cookie = regsuball(req.http.Cookie, "wp-settings-time-1=[^;]+(; )?", "");
	# Remove the wp test cookie
	set req.http.Cookie = regsuball(req.http.Cookie, "wordpress_test_cookie=[^;]+(; )?", "");

    


 if (req.http.Authorization || req.method == "POST") {
        return (pass);
    }

if(
req.http.host == "adm1n.kenweego.com" ||
req.http.host == "dev.kenweego.com" ||
req.url ~ "^/admin(.*)" ||
req.url ~ "^/REST(.*)" ||
req.url ~ "^/v2/REST(.*)" ||
req.url ~ "^/startup-life(.*)" ||
req.url ~ "^/api(.*)" ||
req.url ~ "^/user(.*)" ||
req.url ~ "^/logout(.*)" ||
req.url ~ "^/login_check(.*)" ||
req.url ~ "^/switch_(.*)"
){
        return (pass);
      }


if (req.url ~ "/(wp-login|wp-admin)") {
    return (pass);
}




}

sub vcl_pass {


# Define the default grace period to serve cached content
  
    # By ignoring any other cookies, it is now ok to get a page
   # unset req.http.Cookie;
   # return (lookup);

}

# sub vcl_pipe {
#     # Note that only the first request to the backend will have
#     # X-Forwarded-For set.  If you use X-Forwarded-For and want to
#     # have it set for all requests, make sure to have:
#     # set bereq.http.connection = "close";
#     # here.  It is not set by default as it might break some broken web
#     # applications, like IIS with NTLM authentication.
#     return (pipe);
# }
# 





# 
# sub vcl_hash {
#     hash_data(req.url);
#     if (req.http.host) {
#         hash_data(req.http.host);
#     } else {
#         hash_data(server.ip);
#     }
#     return (hash);
# }
# 
# sub vcl_hit {
#     return (deliver);
# }
# 
# sub vcl_miss {
#     return (fetch);
# }

 sub vcl_backend_response {
  # remove some headers we never want to see
    unset beresp.http.Server;
    unset beresp.http.X-Powered-By;
    
    
    if (beresp.http.content-type ~ "text") {
        set beresp.do_gzip = true;
    }
    
    
    if ( beresp.status != 200 ) {
	    set beresp.uncacheable = true;
    }
    
    

     
    # If our backend returns 5xx status this will reset the grace time
    # set in vcl_recv so that cached content will be served and 
    # the unhealthy backend will not be hammered by requests
    if (beresp.status == 500) {
        set beresp.grace = 60s;
        return (retry);
    }


  if ( bereq.method == "POST" || bereq.http.Authorization ) {
			set beresp.uncacheable = true;
    }
    


     if (beresp.ttl <= 0s ||
         beresp.http.Set-Cookie ||
         beresp.http.Vary == "*") {
 		 	set beresp.uncacheable = true;
 		 	set beresp.ttl = 360 s;
     }
     
     
     set beresp.ttl = 6 h;
     
    
     return (deliver);

 }

 
 sub vcl_deliver {

	if (obj.hits > 0) { 
		set resp.http.X-Cache = "cached";
	} else {
		set resp.http.x-Cache = "uncached";
	}

	unset resp.http.Via;
    unset resp.http.X-Varnish;
     return (deliver);
 }

sub vcl_backend_error {
    if (beresp.status == 503) {
                # set obj.http.location = req.http.Location;
                set beresp.status = 404;
       # set resp.response = "Not Found";
                return (deliver);
    }

	else  if (beresp.status == 750) { 
		set beresp.http.Location = beresp.status;
		set beresp.http.status = 301; 
				set beresp.status = 301; 
		return(deliver); 
}
}


# 
# sub vcl_error {
#     set obj.http.Content-Type = "text/html; charset=utf-8";
#     set obj.http.Retry-After = "5";
#     synthetic {"
# <?xml version="1.0" encoding="utf-8"?>
# <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
#  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
# <html>
#   <head>
#     <title>"} + obj.status + " " + obj.response + {"</title>
#   </head>
#   <body>
#     <h1>Error "} + obj.status + " " + obj.response + {"</h1>
#     <p>"} + obj.response + {"</p>
#     <h3>Guru Meditation:</h3>
#     <p>XID: "} + req.xid + {"</p>
#     <hr>
#     <p>Varnish cache server</p>
#   </body>
# </html>
# "};
#     return (deliver);
# }
# 
# sub vcl_init {
# 	return (ok);
# }
# 
# sub vcl_fini {
# 	return (ok);
# }

