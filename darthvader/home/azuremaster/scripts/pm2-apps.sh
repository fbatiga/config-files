#!/bin/bash
pm2 start -f /apps/apiProd/app.js --name api --merge-logs --log /var/log/apiProd/api1-combined.log
pm2 start -f /apps/apiProd-2/app.js --name api2 --merge-logs --log /var/log/apiProd/api2-combined.log -- --port 1976
pm2 start -f /apps/apiProd-3/app.js --name api3 --merge-logs --log /var/log/apiProd/api3-combined.log -- --port 1977
pm2 start -f /apps/apiProd-vyvop/app.js --name api-vyvop --merge-logs --log /var/log/apiProd/vyvop-combined.log -- --port 1978
#pm2 start -f /apps/yellowStart/app.js --name yellowstart -- --port 2016
