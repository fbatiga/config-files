# You may add here your
# server {
#	...
# }
# statements for each of your virtual hosts to this file

##
# You should look at the following URL's in order to grasp a solid understanding
# of Nginx configuration files in order to fully unleash the power of Nginx.
# http://wiki.nginx.org/Pitfalls
# http://wiki.nginx.org/QuickStart
# http://wiki.nginx.org/Configuration
#
# Generally, you will want to move this file somewhere, and start with a clean
# file but keep this around for reference. Or just disable in sites-enabled.
#
# Please see /usr/share/doc/nginx-doc/examples/ for more detailed examples.
##

server {
	listen   80; ## listen for ipv4; this line is default and implied
	listen	8080;
		#listen   [::]:80 default ipv6only=on; ## listen for ipv6

     include /etc/nginx/user_agents_block.lst;
	root /apps/villa.vyvop.com;

access_log  /var/log/nginx/villa_access.log;
error_log  /var/log/nginx/villa_error.log debug;
	
index index.php index.html index.htm index_prod.php;

	# Make site accessible from http://localhost/
	server_name villa.vyvop.com;
	

	#error_page 404 /404.html;


#do not log robot.txt file
        location = /robots.txt {
                log_not_found off;
                access_log off;
        }

        location / {
                try_files $uri $uri/ /index.php?$query_string;
        }




 location ~ [^/]\.php(/|$) {
                fastcgi_split_path_info ^(.+?\.php)(/.*)$;
                if (!-f $document_root$fastcgi_script_name) {
                        return 404;
                }

		fastcgi_pass php-fpm.sock;
                fastcgi_index index.php;
fastcgi_read_timeout 1200;
                include fastcgi_params;
        }




#	location ~ \.php$ {
#		fastcgi_split_path_info ^(.+\.php)(/.+)$;
#		# NOTE: You should have "cgi.fix_pathinfo = 0;" in php.ini
	
		# With php5-cgi alone:
#		fastcgi_pass 127.0.0.1:9000;
		# With php5-fpm:
#		fastcgi_pass unix:/var/run/php-fpm.sock;
#		fastcgi_index index.php;
#		include fastcgi_params;
#	}

	# deny access to .htaccess files, if Apache's document root
	# concurs with nginx's one
	#
	#location ~ /\.ht {
	#	deny all;
	#}
}


#
#server {
#	listen 443;
#	server_name localhost;
#
#	root html;
#	index index.html index.htm;
#
#	ssl on;
#	ssl_certificate cert.pem;
#	ssl_certificate_key cert.key;
#
#	ssl_session_timeout 5m;
#
#	ssl_protocols SSLv3 TLSv1;
#	ssl_ciphers ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv3:+EXP;
#	ssl_prefer_server_ciphers on;
#
#	location / {
#		try_files $uri $uri/ /index.html;
#	}
#}

