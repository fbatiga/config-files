server {
	listen   80; ## listen for ipv4; this line is default and implied
	listen	8080;
		#listen   [::]:80 default ipv6only=on; ## listen for ipv6

	root /apps/colette.enyosolutions.com/web;

access_log  /var/log/nginx/colette_access.log;
error_log  /var/log/nginx/colette_error.log debug;
	
index index.html index.htm index_prod.php index.php;

	# Make site accessible from http://localhost/
	server_name colette.enyosolutions.com;

	#do not log robot.txt file
        location = /robots.txt {
                log_not_found off;
                access_log off;
        }

        location / {
                try_files $uri $uri/ /index_prod.php?$query_string;
        }

 location ~ [^/]\.php(/|$) {
                fastcgi_split_path_info ^(.+?\.php)(/.*)$;
                if (!-f $document_root$fastcgi_script_name) {
                        return 404;
                }

		fastcgi_pass php-fpm.sock;
                fastcgi_index index_prod.php;
fastcgi_read_timeout 1200;
                include fastcgi_params;
        }




}
