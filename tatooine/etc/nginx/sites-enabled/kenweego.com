server {
    server_name www.kenweego.com;
    return 301 $scheme://kenweego.com$request_uri;
}
server {
    server_name blog.kenweego.com;
    return 301 $scheme://kenweego.com/news$request_uri;
}

server {
	listen   80; ## listen for ipv4; this line is default and implied
	listen	8080;
		#listen   [::]:80 default ipv6only=on; ## listen for ipv6

     include /etc/nginx/user_agents_block.lst;

	root /apps/kenweego.com/web;



	access_log  /var/log/nginx/kenweego.com_access.log;
	error_log  /var/log/nginx/kenweego.com_error.log debug;



	index	index.html	index_prod.php index.php;

	# Make site accessible from http://localhost/
	server_name kenweego.com www2.kenweego.com;

	# do not log favicon
	location = /favicon.ico {
                log_not_found off;
                access_log off;
        }
 
	#do not log robot.txt file
        location = /robots.txt {
                log_not_found off;
                access_log off;
        }

	location / {
		try_files $uri $uri/ /index_prod.php?$query_string;
	}






	#error_page 404 /404.html;

        include h5bp/basic.conf;

	include alias/api;
	include alias/feedback;
	include alias/blog;






 location ~ [^/]\.php(/|$) {
                fastcgi_split_path_info ^(.+?\.php)(/.*)$;
                if (!-f $document_root$fastcgi_script_name) {
                        return 404;
                }
 
                #fastcgi_pass 127.0.0.1:9000;
		fastcgi_read_timeout 150;
		include fastcgi_params;
#		fastcgi_pass php-fpm.sock;
		fastcgi_pass unix:/var/run/php/php5.6-fpm.sock;
                fastcgi_index index_prod.php;
        }


location ~* ^/(img|js|data)/.+\.(ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf)$ {
       access_log off; log_not_found off; expires 1M;
}


location ~* ^/(img|js)/.+\.(js|css)$
{
       access_log off; log_not_found off; expires 1w;
}


}
