#!/bin/bash
    apt-get update && \
    apt-get install -y python-software-properties software-properties-common apt-utils 
    
    export LC_ALL=C.UTF-8 
export LANG=en_US.UTF-8
export LANGUAGE=en_US
export LC_ALL=en_US.UTF-8
locale-gen

add-apt-repository ppa:ondrej/php 
    cat /dev/zero | ssh-keygen -q -N ""
    apt-get update 
    apt-get install nginx && \
    apt-get install -y  nano sudo git logrotate zip unzip htop php5.6-cli php5.6-fpm php5.6-bcmath php5.6-mongodb php5.6-redis && \
    apt-get install -y  php5.6-bz2 php5.6-dba php5.6-imap php5.6-intl php5.6-mcrypt php5.6-soap php5.6-tidy && \
    apt-get install -y  php5.6-common php5.6-curl php5.6-gd php5.6-mysql php5.6-xml php5.6-zip php5.6-gettext php5.6-mbstring && \
    mkdir /tmp/composer/ && \
    cd /tmp/composer && \
    curl -sS https://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer && \
    chmod a+x /usr/local/bin/composer && \
    cd / && \
    rm -rf /tmp/composer && \
    apt-get remove -y python-software-properties software-properties-common && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* 

    sed -i -e '/s%admin	ALL=(ALL) ALL/%admin	ALL=(ALL) PASSWORD: ALL/' /etc/sudoers
    useradd -m -p -s /bin/bash azuremaster 
    cd /home/azuremaster
    git clone git@gitlab.com:kenweego/config-files.git
    cd config-files
    cp -R tatooine/* /
    chown -R azuremaster:azuremaster /home/azuremaster
    rm -rf /etc/nginx/sites-enabled/default /etc/nginx/sites-available/default && \
    sed -i -e 's/^user = www-data$/;user = www-data/g' /etc/php/5.6/fpm/pool.d/www.conf && \
    sed -i -e 's/^group = www-data$/;group = www-data/g' /etc/php/5.6/fpm/pool.d/www.conf && \
    sed -i -e 's/^listen.owner = www-data$/;listen.owner = www-data/g' /etc/php/5.6/fpm/pool.d/www.conf && \
    sed -i -e 's/^listen.group = www-data$/;listen.group = www-data/g' /etc/php/5.6/fpm/pool.d/www.conf && \
    sed -i -e 's/fastcgi_param  SERVER_PORT        $server_port;/fastcgi_param  SERVER_PORT        $http_x_forwarded_port;/g' /etc/nginx/fastcgi_params && \
    mkdir --mode 777 /var/run/php && \
    chmod 755 /hooks /var/www && \
    chmod -R 777 /var/www/html /var/log && \
    chmod 666 /etc/nginx/sites-enabled/ /etc/passwd /etc/group && \
    mkdir /apps && chmod 777 /apps
