#!/bin/bash
rm /tmp/import*

echo "Downloading expedia datafeed $now"
mkdir /tmp/hotels
cd /tmp/hotels

rm ActivePropertyList*
wget "https://www.ian.com/affiliatecenter/include/V2/ActivePropertyList.zip"
unzip -o ActivePropertyList.zip

echo "Downloading expedia images $now"

rm HotelImageList*
wget "https://www.ian.com/affiliatecenter/include/V2/HotelImageList.zip" -O HotelImageList.zip
unzip -o HotelImageList.zip


echo "Downloading expedia descriptions  $now"
rm PropertyDescriptionList*
wget "https://www.ian.com/affiliatecenter/include/V2/PropertyDescriptionList.zip" -O PropertyDescriptionList.zip
unzip -o PropertyDescriptionList.zip

echo "Downloading expedia descriptions  $now"
rm PropertyDescriptionList_fr_FR*
wget "https://www.ian.com/affiliatecenter/include/V2/PropertyDescriptionList_fr_FR.zip" -O PropertyDescriptionList_fr_FR.zip
unzip -o PropertyDescriptionList_fr_FR.zip
