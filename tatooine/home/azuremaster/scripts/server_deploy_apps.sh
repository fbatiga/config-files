#!/bin/bash
sudo su
mkdir /apps && cd apps
mkdir admin.kenweego.com  blog.kenweego.com  blog.vyvop  enyosolutions.com  feedback  kenweego.com  mongoadmin  redisadmin  rockmongo  villa.vyvop.com  vyvop  vyvop-villa-website
cd /apps/kenweego.com &&  git clone --recursive git@gitlab.com:kenweego/kwg-front.git .
touch resources/logs/app.log
chmod -R 777 resources/logs resources/cache
touch resources/routes_cms.php
echo "<?php && \
$app->get("/en/about","page.controller:defaultAction")->bind("about_page_en"); && \
$app->get("/fr/qui-sommes-nous","page.controller:defaultAction")->bind("page_theteam_fr");  && \ 
$app->get("/de/privacy","page.controller:defaultAction")->bind("about_page_de");  && \
$app->get("/es/privacidad","page.controller:defaultAction")->bind("privacy_policy_es");  && \
$app->get("/fr/privacy","page.controller:defaultAction")->bind("privacy_policy_fr");  && \
$app->get("/en/privacy","page.controller:defaultAction")->bind("privacy_policy_en");  && \
$app->get("/fr/a-propos","page.controller:defaultAction")->bind("about_page_fr");  && \
$app->get("/es/acerca-de-nosotros","page.controller:defaultAction")->bind("about_page_es");  && \
$app->get("/en/who-are-we","page.controller:defaultAction")->bind("page_theteam_en"); " >  resources/routes_cms.php
composer update


cd /apps/admin.kenweego.com &&  git clone --recursive git@gitlab.com:kenweego/kwg-back.git .
touch resources/logs/app.log
chmod -R 777 resources/logs resources/cache
composer update

cd /apps/vyvop &&  git clone git@gitlab.com:vyvop/vyvop-landing-website.git .
touch resources/logs/app.log
chmod -R 777 resources/logs resources/cache
composer update

cd /apps/villa.vyvop.com && git clone  git@gitlab.com:vyvop/vyvop-villa-website.git . 
composer update
